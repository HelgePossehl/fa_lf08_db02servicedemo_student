package de.szut.springboot_db02_service_demo.controller;

import de.szut.springboot_db02_service_demo.mapper.ArticleMapper;
import de.szut.springboot_db02_service_demo.mapper.SupplierMapper;
import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.model.Supplier;
import de.szut.springboot_db02_service_demo.request.SupplierRequest;
import de.szut.springboot_db02_service_demo.response.ArticleResponse;
import de.szut.springboot_db02_service_demo.response.SupplierResponse;
import de.szut.springboot_db02_service_demo.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/store/suppliers")
@RequiredArgsConstructor
public class SupplierController {

    //private Logger logger = LoggerFactory.getLogger(SupplierController.class);

    private final SupplierService service;
    private final SupplierMapper mapper;
    private final ArticleMapper mapperArticle;

    @PostMapping
    public SupplierResponse addSupplier(@Valid @RequestBody SupplierRequest request) {
        Supplier supplierRequest = mapper.mapRequestToModel(request);
        Supplier supplierResponse = service.create(supplierRequest);
        return mapper.mapModelToResponse(supplierResponse);
    }

    @GetMapping(value = "/{id}")
    public SupplierResponse getSupplier(@PathVariable long id) {
        Supplier supplierResponse = service.readById(id);
        return mapper.mapModelToResponse(supplierResponse);
    }

    @GetMapping
    public List<SupplierResponse> getAllSuppliers() {
        List<Supplier> supplierList = service.readAll();
        List<SupplierResponse> responseList = new ArrayList<>();
        for (Supplier supplier : supplierList) {
            responseList.add(mapper.mapModelToResponse(supplier));
        }
        return responseList;
    }

    @GetMapping (value = "/{id}/articles")
    public List<ArticleResponse> getForOneSupplierAllArticles(@PathVariable long id) {
        List<Article> articleList = service.readForOneSupplierAllArticles(id);
        List<ArticleResponse> articleResponseList = new ArrayList<>();
        for(Article article : articleList) {
            articleResponseList.add(mapperArticle.mapModelToResponse(article));
        }
        return articleResponseList;
    }

    @PutMapping(value = "/{id}")
    public SupplierResponse updateSupplier(@PathVariable long id, @Valid @RequestBody SupplierRequest request) {
        Supplier supplierRequest = mapper.mapRequestToModel(id, request);
        Supplier supplierResponse = service.update(supplierRequest);
        return mapper.mapModelToResponse(supplierResponse);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteSupplier(@PathVariable long id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
