package de.szut.springboot_db02_service_demo.controller;

import de.szut.springboot_db02_service_demo.mapper.ArticleMapper;
import de.szut.springboot_db02_service_demo.mapper.SupplierMapper;
import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.model.Supplier;
import de.szut.springboot_db02_service_demo.request.ArticleRequest;
import de.szut.springboot_db02_service_demo.response.ArticleResponse;
import de.szut.springboot_db02_service_demo.response.SupplierResponse;
import de.szut.springboot_db02_service_demo.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/store/articles")
@RequiredArgsConstructor
public class ArticleController {

    private final ArticleService service;
    private final ArticleMapper mapper;
    private final SupplierMapper mapperSupplier;

    @PostMapping
    public ArticleResponse addArticle(@Valid @RequestBody ArticleRequest request) {
        Article articleRequest = mapper.mapRequestToModel(request);
        Article articleResponse = service.create(articleRequest);
        return mapper.mapModelToResponse(articleResponse);
    }

    @GetMapping(value = "/{id}")
    public ArticleResponse getArticle(@PathVariable long id) {
        Article articleResponse = service.readById(id);
        return mapper.mapModelToResponse(articleResponse);
    }

    @GetMapping(value = "/{id}/suppliers")
    public SupplierResponse getForOneArticleItsSupplier(@PathVariable long id) {
        Supplier supplierResponse = service.readForOneArticleItsSupplier(id);
        return mapperSupplier.mapModelToResponse(supplierResponse);
    }

    @GetMapping
    public List<ArticleResponse> getAllArticles() {
        List<Article> articleList = service.readAll();
        List<ArticleResponse> articelResponseList = new ArrayList<>();
        for(Article article : articleList) {
            articelResponseList.add(mapper.mapModelToResponse(article));
        }
        return articelResponseList;
    }

    @PutMapping(value = "/{id}")
    public ArticleResponse updateArticle(@PathVariable long id, @Valid @RequestBody ArticleRequest request) {
        Article articleRequest = mapper.mapRequestToModel(id, request);
        Article articleResponse = service.update(articleRequest);
        return mapper.mapModelToResponse(articleResponse);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteArticle(@PathVariable long id) {
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
