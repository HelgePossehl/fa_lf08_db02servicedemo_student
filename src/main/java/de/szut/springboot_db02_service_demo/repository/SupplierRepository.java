package de.szut.springboot_db02_service_demo.repository;

import de.szut.springboot_db02_service_demo.model.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {
}
