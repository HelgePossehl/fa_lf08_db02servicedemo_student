package de.szut.springboot_db02_service_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDb02ServiceDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDb02ServiceDemoApplication.class, args);
    }

}
