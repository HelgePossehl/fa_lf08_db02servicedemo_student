package de.szut.springboot_db02_service_demo.request;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
public class SupplierRequest {

    @NotBlank(message = "Name is mandatory.")
    @Size(max = 50, message = "Name must not have more than 50 characters.")
    private String name;
    private String phone;
    @NotBlank(message = "Email is mandatory.")
    private String email;

}
