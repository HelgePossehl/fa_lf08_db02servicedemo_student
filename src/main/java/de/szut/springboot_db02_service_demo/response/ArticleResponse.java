package de.szut.springboot_db02_service_demo.response;

import lombok.Data;

@Data
public class ArticleResponse {

    private long id;
    private String designation;
    private double price;

}
