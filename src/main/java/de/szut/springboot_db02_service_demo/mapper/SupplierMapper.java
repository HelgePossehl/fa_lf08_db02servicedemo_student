package de.szut.springboot_db02_service_demo.mapper;

import de.szut.springboot_db02_service_demo.model.Supplier;
import de.szut.springboot_db02_service_demo.request.SupplierRequest;
import de.szut.springboot_db02_service_demo.response.SupplierResponse;
import de.szut.springboot_db02_service_demo.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SupplierMapper {

    private final SupplierService service;

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public Supplier mapRequestToModel(SupplierRequest request) {
        if (request == null) { return null; }
        Supplier model = new Supplier();
        model.setId(0);
        model.setName(request.getName());
        model.setPhone(request.getPhone());
        model.setEmail(request.getEmail());
        return model;
    }

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param id        Die ID des Request-Objekts
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public Supplier mapRequestToModel(long id, SupplierRequest request) {
        if (request == null) { return null; }
        Supplier model = mapRequestToModel(request);
        model.setId(id);
        model.setArticleList(service.readForOneSupplierAllArticles(id));
        return model;
    }

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt
     * @param model Das Model-Objekt.
     * @return  Das Response-Objekt.
     */
    public SupplierResponse mapModelToResponse(Supplier model) {
        if (model == null) { return null; }
        SupplierResponse response = new SupplierResponse();
        response.setId(model.getId());
        response.setName(model.getName());
        response.setPhone(model.getPhone());
        response.setEmail(model.getEmail());
        return response;
    }

}
