package de.szut.springboot_db02_service_demo.mapper;

import de.szut.springboot_db02_service_demo.model.Article;
import de.szut.springboot_db02_service_demo.request.ArticleRequest;
import de.szut.springboot_db02_service_demo.response.ArticleResponse;
import de.szut.springboot_db02_service_demo.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ArticleMapper {

    private final SupplierService supplierService;

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public Article mapRequestToModel(ArticleRequest request) {
        if (request == null) { return null; }
        Article model = new Article();
        model.setDesignation(request.getDesignation());
        model.setPrice(request.getPrice());
        if (request.getSupplierId() > 0) {
            model.setSupplier(supplierService.readById(request.getSupplierId()));
        }
        return model;
    }

    /**
     * Mapped ein Request-Objekt in ein Model-Objekt.
     * @param id        Die ID des Request-Objekts
     * @param request   Das Request-Objekt.
     * @return  Das Model-Objekt.
     */
    public Article mapRequestToModel(long id, ArticleRequest request) {
        if (request == null) { return null; }
        Article model = mapRequestToModel(request);
        model.setId(id);
        return model;
    }

    /**
     * Mapped ein Model-Objekt in ein Response-Objekt.
     * @param model Das Model-Objekt.
    * @return  Das Response-Objekt.
     */
    public ArticleResponse mapModelToResponse(Article model) {
        if (model == null) { return null; }
        ArticleResponse response = new ArticleResponse();
        response.setId(model.getId());
        response.setDesignation(model.getDesignation());
        response.setPrice(model.getPrice());
        return response;
    }

}
